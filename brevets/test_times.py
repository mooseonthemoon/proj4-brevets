from acp_times import open_time, close_time
import arrow

def test_cases():

	startArrow = arrow.get("2017-01-01 00:00-08:00", "YYYY-MM-DD hh:mmZZ")
	endArrow = arrow.get("2017-01-01 01:00-08:00", "YYYY-MM-DD hh:mmZZ")

	assert open_time(0, 200, startArrow.isoformat()) == startArrow.isoformat()
	assert close_time(0, 200, startArrow.isoformat()) == endArrow.isoformat()
	assert open_time(0, 200, startArrow.isoformat()) == startArrow.isoformat()
	assert close_time(0, 200, startArrow.isoformat()) == endArrow.isoformat()
	assert close_time(0, 1000, startArrow.isoformat()) == endArrow.isoformat()