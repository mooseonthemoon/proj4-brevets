##High Level

This algorithm is developed from the ACP control times

The website to calculate all of the times is https://rusa.org/pages/acp-brevet-control-times-calculator
and with info here https://rusa.org/pages/rulesForRiders

This calculator, as described in lecture, is a good model to base my algorithm on https://rusa.org/octime_acp.html

I'm using AJAX and Flask for this application and the calculator

##Running

Use the scripts provided in the /brevets directory: ./run.sh will build the image and the other is cleanup

##Clearing ambiguity

I'm not letting times above the local max brevet count, they just default to that max brevet, I'm not doing
much rounding either as the calculator is doing, the times are a bit more arbitrary but shouldn't mess up testing.

##User vs Developer

As a user, if you're planning on making another route, or you have messed up your current route, it's best to 
refresh the page and start over. There's nothing really for developers to know, the front end is whole, just be sure to refresh as well.

Name: Will Christensen
Email: wwc@uoregon.edu
